<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Types\ImageType;
use AppBundle\Types\UpdateImageType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation as Doc;


/**
 * @Route("/ImagesRestServices", name="imagesWebService")
 */
class ApiController extends Controller
{


    /**
     * @Rest\Get("/", name="Api_show_All")
     * @ApiDoc(ressource=true,
     * description="This service allow us to display all images available in our database",
     *section="Images-Manager",
     *     statusCodes={200="returned when operation terminated successfully",
     *     400="returned when a violation is raised by validation"}
     *)
     */
    public function showAllImagesAction(Request $request)
    {

        $list = $this->getDoctrine()->getManager();
        $images = $list->getRepository('AppBundle:Image')->findAll();

        if (empty($images)) {
            return new JsonResponse(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
        }
        $formatted = [];
        foreach ($images as $image) {
            $formatted[] = [
                'id' => $image->getId(),
                'titre' => $image->getTitre(),
                'description' => $image->getDescription(),
                'url' => $image->getUrl()
            ];
        }

        return new JsonResponse($formatted);

    }

    /**
     * @Rest\Post("/Add", name="Api_add_image")
     * @ApiDoc(ressource=true,
     *     description="Use this service to add a new image and send it to the database",
     *     section="Images-Manager",
     * requirements={
     *     {
     *
     *     "name"="id",
     *     "dataType"="integer",
     *     "requirements"="\d+",
     *     "description"="The image unique identified."},
     *     {
     *     "name"="titre",
     *     "dataType"="string",
     *     "requirements"="\d+",
     *     "description"="The image title."
     *
     *     },
     *     {"name"="description",
     *     "dataType"="string",
     *    "requirements"="\d+",
     *     "description"="The image description"
     *         },
     *
     *     {"name"="url",
     *     "dataType"="string",
     *     "requirements"="\d+",
     *     "description"="The image url"
     *         }
     *     },
     *
     *
     *
     *
     *     statusCodes={ 200="Returned when created",
     *     400="returned when a violation is raised by validation"   })
     *
     *
     */
    public function addAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $image = new Image();
        $image->setTitre($request->get("titre"));
        $image->setDescription($request->get("description"));
        $image->setUrl($request->get("url"));
        $em->persist($image);
        $em->flush();


        return new JsonResponse(['message' => 'Image created']);
    }

    /**
     * @Rest\Put("/Update/{id}", name="Api_update_image")
     * @ApiDoc(description="Use this service to update an existing image and resend it to the database",
     *         section="Images-Manager",
     *
     *     requirements={{"name"="id",
     *   "dataType"="integer",
     * "requirements"="\d+",
     *     "description"="the unique identifier is required to execute this service"
     *        }},
     *     statusCodes={200="Returned when operation is successfully executed",
     *     400="returned when a violation is raised by validation"}
     *
     *      )
     */
    public function updateAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $image = $em->getRepository("AppBundle:Image")->find($id);
        $image->setTitre($request->get("titre"));
        $image->setDescription($request->get("description"));
        $image->setUrl($request->get("url"));
        $em->persist($image);
        $em->flush();
        return new JsonResponse(['message' => 'Image Updated']);
    }

    /**
     * @Rest\Delete("/Delete/{id}", name="Api_deleteimage")
     * @ApiDoc(description="Use this service to delete an existing image",
     *     section="Images-Manager",
     *     requirements={
     *        {"name"="id",
     *     "dataType"="integer",
     *     "requirements"="\d+",
     *     "description"="The unique identifier is required to execute this operation"
     *        }},
     *
     * statusCodes={200="Returned when operation is successfully executed",
     *     400="returned when a violation is raised by validation"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('AppBundle:Image')->find($id);
        $em->remove($image);
        $em->flush();
        new JsonResponse(['message' => 'Image deleted']);
    }
}
