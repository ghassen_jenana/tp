<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Types\ImageType;
use AppBundle\Types\UpdateImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function showAllImagesAction(Request $request)
    {

        $list=$this->getDoctrine()->getManager();
        $images=$list->getRepository('AppBundle:Image')->findAll();
      return $this->render('AppBundle:Images:imageListing.html.twig', array('images'=>$images));
    }
    /**
     * @Route("/Add", name="addimage")
     */
    public function addAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $image= new Image();
        $form = $this->createForm( ImageType::class, $image);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($image);
            $em->flush();
            return $this->redirectToRoute("homepage");
        }
        return $this->render('AppBundle:Images:Add.html.twig', array(
            'form' => $form->createView()
        ));
    }
    /**
     * @Route("/Update/{id}", name="updateimage")
     */
    public function updateAction(Request $request,$id){

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('AppBundle:Image')->find($id);


        $form = $this->createForm(UpdateImageType::class, $image);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($image);
            $em->flush();
            return $this->redirectToRoute("homepage");
        }

        return $this->render('AppBundle:Images:Update.html.twig', array(
            'form' => $form->createView()
        ));
    }
    /**
     * @Route("/Delete/{id}", name="deleteimage")
     */
    public function deleteAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('AppBundle:Image')->find($id);
        $em->remove($image);
        $em->flush();
        return $this->redirectToRoute("homepage");
    }
}
